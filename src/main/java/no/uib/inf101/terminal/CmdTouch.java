package no.uib.inf101.terminal;
import java.io.File;
import java.io.IOException;

public class CmdTouch implements Command {
    Context context;

    @Override
    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public String run(String[] args) {

        File file = new File(args[0]);
        if (file.exists()){
            return String.format("File named %s allready exists.", args[0]);
        }
        try {
            file.createNewFile();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return String.format("New file %s created.", args[0]);

    }

    @Override
    public String getName() {
        return "touch";
    }

    @Override
    public String getManual() {
        return "Touch + filnavn oppretter en ny fil i cd.";
    }
    
}
