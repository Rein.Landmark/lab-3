package no.uib.inf101.terminal;

// UiB INF101 ShellLab - Main.java
// Dette er filen som inneholder main-metoden.

public class Main {

//README: Nekter å gjøre rm-filename delen samt mv-filename delen, orker ikke slette/flytte hele oppgaven med et uhell :)
//GREP er implementert som "grep filename searchword" for enkelthetens skyld
//returnerer linjenummer til ordet

  public static void main(String[] args) {
    // Create a new shell
    SimpleShell shell = new SimpleShell();

    // Installing commands
    shell.installCommand(new CmdEcho());
    shell.installCommand(new CmdExit());
    shell.installCommand(new CmdJavaProgram("hello", HelloWorld.class));
    shell.installCommand(new CmdJavaProgram("new", Main.class)); // åpner et nytt vindu, kjører programmet en gang til
    


    // Run the shell in the terminal GUI
    Terminal gui = new Terminal(shell);
    gui.run();
  }
}