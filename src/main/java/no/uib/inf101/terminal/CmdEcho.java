package no.uib.inf101.terminal;

public class CmdEcho implements Command {

    @Override
    public String run(String[] args) {
        String result = "";
        for (String element : args){
            result += element + " ";
        }
        return result;
    }

    @Override
    public String getName() {
        return "echo";
    }

    @Override
    public String getManual() {
        return "dont use tis command";
    }
    
}
