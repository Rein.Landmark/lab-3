package no.uib.inf101.terminal;

import java.util.Map;

public interface Command {

    String run(String[] args);

    String getName();

    default void setContext(Context context) { /* do nothing */ };

    String getManual(); //Returnerer instruksjoner om kommandoen

    default void setCommandContext(Map<String, Command> commands){
    }

}
