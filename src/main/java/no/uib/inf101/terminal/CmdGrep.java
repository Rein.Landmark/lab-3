package no.uib.inf101.terminal;

import java.util.List;

public class CmdGrep implements Command{

    Context context;
    public void setCommandContext(Command command){
        this.context = context;
    }

    @Override
    public String run(String[] args) {
        CmdCat read = new CmdCat();
        String content = read.run(args);

        List<String> contentList = content.lines().toList();
        int counter = 1;
        for (String line : contentList){
            if (line.contains(args[1])){
                return Integer.toString(counter);
            } counter += 1;
        }
        return "File does not contain searchword.";
    }

    @Override
    public String getName() {
        return "grep";
    }

    @Override
    public String getManual() {
        return "Tells you the line of the first appearance of the searchword.";
    }
    
}
