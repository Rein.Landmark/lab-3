package no.uib.inf101.terminal;

import java.io.File;

public class CmdMkdir implements Command{

    Context context;
    public void setCommandContext(Context context){
        this.context = context;
    }

    @Override
    public String run(String[] args) {
        File directory = new File(args[0]);
        if (directory.exists()){
            return String.format("Thie directory %s allready exists.", args[0]);
        }
        directory.mkdirs();
        return String.format("Directories named %s created.", args[0]);
        
    }

    @Override
    public String getName() {
        return "mkdir";
    }

    @Override
    public String getManual() {
        return "Makes a folder. Can create subfolders: mkdir folder1/folder2/folder3";
    }
    
}
