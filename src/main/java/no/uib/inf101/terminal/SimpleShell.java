package no.uib.inf101.terminal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


// TODO 1: Let SimpleShell implement CommandLineInterface
public class SimpleShell implements CommandLineInterface{


  /** The prompt to show before each command */
  private final String prompt = "$ ";
  /** The context variable contains cwd and home directories etc. */
  private final Context context = new Context();
  /** A list of historic commands and their outputs */
  private final List<String> history = new ArrayList<>();
  /** The command currently being typed */
  private String currentCommand = "";
  // TODO 4: Create instance variable storing Command objects
  private final HashMap<String, Command> allCommands = new HashMap<>();
  
  

  public void installCommand(Command command){

    this.allCommands.put(command.getName(), command);
    command.setContext(this.context);
    command.setCommandContext(this.allCommands);
  }
  

  public SimpleShell() {

    this.installCommand(new CmdLs());
    this.installCommand(new CmdCd());
    this.installCommand(new CmdPwd());
    this.installCommand(new CmdMan());
    this.installCommand(new CmdTouch());
    this.installCommand(new CmdMkdir());
    this.installCommand(new CmdCat());
    this.installCommand(new CmdGrep());
  }


  public void keyPressed(char key) {
    if (key == 8 && this.currentCommand.length() >= 1) {
      this.currentCommand = this.currentCommand.substring(0, (this.currentCommand.length() - 1));
    }
    if (key == '\n') {
      this.processCurrentCommandLine();
    } else if (key >= ' ' && key <= '~') {
      this.currentCommand += key;
    } else {
      // Some special key was pressed (e.g. shift, ctrl), we ignore it
    }
  }

  public String getScreenContent() {
    String s = "";
    for (String line : this.history) {
      s += line;
    }
    s += this.prompt;
    s += this.currentCommand;
    return s;
  }

  private void processCurrentCommandLine() {
    String result = "";
    if (this.currentCommand.length() > 0) {
      String[] args = this.currentCommand.split(" ");
      String commandName = args[0];
      String[] commandArgs = new String[args.length - 1];
      System.arraycopy(args, 1, commandArgs, 0, commandArgs.length);
      result = this.executeCommand(commandName, commandArgs);
      if (result.length() > 0 && result.charAt(result.length() - 1) != '\n') {
        result += '\n';
      }
    }
    this.history.add(this.prompt + this.currentCommand + "\n" + result);
    this.currentCommand = "";
  }

  private String executeCommand(String commandName, String[] args) {

    Command command = this.allCommands.get(commandName);

    if (command != null){
      return command.run(args);
    }
    else return "Command not found: \"" + commandName + "\"";
        }

}
