package no.uib.inf101.terminal;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class CmdCat implements Command {

    Context context;
    public void setCommandContext(Context context){
        this.context = context;
    }
    

    @Override
    public String run(String[] args){
        try (BufferedReader read = new BufferedReader(
            new InputStreamReader(
                new FileInputStream(args[0]),
                StandardCharsets.UTF_8))){
        String output = "";

        List<String> input = read.lines().toList();

        for (String line : input){
            output += line + "\n";
        }
        return output.strip();
    }catch (IOException e) {
    
        e.printStackTrace();
        return "Unknown Error.";
    }
    }

    @Override
    public String getName() {
        return "cat";
    }

    @Override
    public String getManual() {
        return "Reads content of file";
    }
    
}
