package no.uib.inf101.terminal;

import java.util.Map;

public class CmdMan implements Command{

    Map<String, Command> manual;

    @Override
    public String run(String[] args) {
        
        if (args == null) {
            return "Invalid request.";
        }
        String commandName = args[0];
        Command command = manual.get(commandName);
        if (command == null) {
            return "Invalid command";
        } 

        return command.getManual();
    }

    @Override
    public String getName() {
        return "man";
    }

    @Override
    public String getManual() {
        return "bruh";
    }

    public void setCommandContext(Map<String, Command> commands) {
        this.manual = commands;
    }
    
}
